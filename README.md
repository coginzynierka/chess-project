## Ogólne
* frontendowa aplikacja znajduje się pod src/main/frontend

## Budowanie i uruchamianie
### Frontend
Uruchamia serwer z watche'em na porcie 3000

* _yarn install_
* _yarn start_

### Backend
Uruchamia server na porcie 8080
* _mvn package_
* _mvn spring-boot:run_

### Produkcyjna paczka
_Zawiera ona skompilowane pliki javowe oraz zbudowaną paczkę frontendową_

* _mvn package -DskipTests_

## Postman
W projekcie pod _/postman_ dołączone są kolekcje postmanowe. Aby z niej skorzystać, należy ją zaimportować w postmanie.


![](postman/static/postman-import.gif)

## Dokumentacja
Dokumentacja dostępna pod linkiem: https://docs.google.com/document/d/103Hbyu6WZFPJnZgw6aIERq2Q1DiY01akDlkTcH9Rc9k/edit?usp=sharing

const apiRoot = "http://localhost:8080/api/"

export const paths = {
  listAllExercises: `${apiRoot}chess-exercise`,
  createExercise: `${apiRoot}chess-exercise`,
  updatExercise: id => `${apiRoot}chess-exercise/${id}`,
  exercise: id => `${apiRoot}chess-exercise/${id}`,
  collections: `${apiRoot}chess-exercise-set`,
  collection: id => `${apiRoot}chess-exercise-set/${id}`,
  image: id => `${apiRoot}chess-exercise/${id}/chess-board`,
  collectionExercises: (collectionId, exerciseId) =>
    `${apiRoot}chess-exercise-set/${collectionId}/exercises`,
  collectionPDF: collectionId => 
    `${apiRoot}chess-exercise-set/${collectionId}/pdf`
};

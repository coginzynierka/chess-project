import { takeEvery, call, put, all, select, delay } from 'redux-saga/effects';
// import { push } from 'react-router-redux';

import {
  listAllExercises,
  createExercise,
  getExerciseData,
  saveCollection,
  listAllSets,
  getCollectionData,
  goToSet,
  addExerciseToCollection,
  getCollectionPDF,
  saveExercise,
  createCollection,
} from '../effects/effects';

import * as constants from '../constants';

import {
  updateExercises,
  setExerciseData,
  toggleIsSavingExercise,
} from '../actions/boardActions';
import { getExerciseShapeFromState } from '../helpers/helpers';
import { setSetsData, requestAllExercises } from '../actions/searchActions';
import {
  setCollectionData,
  requestCollectionData,
  toggleSaveCollectionConfirmation,
  saveCollection as saveColllectionAction,
} from '../actions/collectionActions';
import { clearQuickCollection } from '../actions/quickCollectionActions';

export function* rootSaga() {
  yield takeEvery(constants.REQUEST_ALL_EXERCISES, function*() {
    const allExercises = yield call(listAllExercises);
    yield put(updateExercises(allExercises));
  });

  yield takeEvery(constants.GET_EXERCISE_DATA, function*() {
    const exerciseId = yield select(state => state.board.exerciseId);
    const exerciseData = yield call(getExerciseData, exerciseId);
    yield put(setExerciseData(exerciseData));
  });

  yield takeEvery(constants.CREATE_EXERCISE, function*({ payload }) {
    const boardState = yield select(state => state.board);

    yield call(createExercise, getExerciseShapeFromState(boardState));
    yield put(toggleIsSavingExercise());
    yield delay(3000);
    yield put(toggleIsSavingExercise());
  });

  yield takeEvery(constants.SAVE_EXERCISE, function*({ payload }) {
    yield put(toggleIsSavingExercise());
    const boardState = yield select(state => state.board);

    yield call(
      saveExercise,
      payload.exerciseId,
      getExerciseShapeFromState(boardState),
    );
    yield delay(500);
    yield put(toggleIsSavingExercise());
  });

  yield takeEvery(constants.SAVE_COLLECTION, function*() {
    const { collection } = yield select(state => state);
    yield put(toggleSaveCollectionConfirmation());
    yield call(saveCollection, collection);
    yield delay(3000);
    yield put(toggleSaveCollectionConfirmation());
  });

  yield takeEvery(constants.CREATE_COLLECTION, function*() {
    const { name, exercises } = yield select(state => state.quickCollection);
    const { id: collectionId } = yield call(createCollection, {
      name,
      exercises,
    });
    yield call(goToSet, collectionId);
  });

  yield takeEvery(constants.REQUEST_ALL_COLECITONS, function*() {
    const allSets = yield call(listAllSets);
    yield put(setSetsData(allSets));
  });

  yield takeEvery(constants.REQUEST_COLLECTION_DATA, function*({ payload }) {
    const collection = yield call(getCollectionData, payload.id);
    yield put(setCollectionData(collection));
  });

  yield takeEvery(constants.CREATE_QUICK_COLLECTION, function*() {
    const { exercises, name } = yield select(state => state.quickCollection);
    const { id } = yield call(createCollection, { name, exercises });
    yield put(clearQuickCollection());

    // TODO: Sync saga with router
    yield call(goToSet, id);
  });

  yield takeEvery(constants.SHOW_QUICK_SEARCH, function*() {
    yield put(requestAllExercises());
  });

  yield takeEvery(constants.ADD_ITEM_TO_COLLECTION, function*({ payload }) {
    const boardState = yield select(state => state.board);

    const { id } = yield call(
      createExercise,
      getExerciseShapeFromState(boardState),
    );

    yield call(addExerciseToCollection, payload.collectionId, [id]);
    yield call(() => window.history.back());
  });

  yield takeEvery(constants.GET_COLLECTION_PDF, function*({ payload }) {
    yield put(saveColllectionAction());
    yield call(getCollectionPDF, payload.collectionId);
  });

  yield takeEvery(
    constants.ADD_QUICK_COLLECTION_TO_EXISTING_COLLECTION,
    function*({ payload }) {
      const exercises = yield select(state => state.quickCollection.exercises);

      yield call(addExerciseToCollection, payload.collectionId, exercises);
      yield put(clearQuickCollection());
      yield call(() => window.history.back());
    },
  );

  yield takeEvery(constants.SWAP_ITEM_IN_COLLECTION, function*({ payload }) {
    const boardState = yield select(state => state.board);

    const [createdExercise, collection] = yield all([
      call(createExercise, getExerciseShapeFromState(boardState)),
      call(getCollectionData, payload.collectionId),
    ]);
    
    const updatedCollection = {
      ...collection,
      exercises: collection.exercises.map(exercise =>
        exercise.id === boardState.exerciseId ? createdExercise : exercise,
      ),
    };

    yield call(saveCollection, updatedCollection);
    yield call(() => window.history.back());
  });
}

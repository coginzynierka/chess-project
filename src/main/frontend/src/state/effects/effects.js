import axios from 'axios';

import { paths } from '../../config';

export const listAllExercises = () => axios
    .get(paths.listAllExercises)
    .then(({ data }) => data);

export const createExercise = requestBody => axios
    .post(paths.createExercise, requestBody)
    .then(({ data }) => data);

export const saveExercise = (id, requestBody) => axios   
    .put(paths.updatExercise(id), requestBody)   
    .then(({ data }) => data); 

export const getExerciseData = id => axios
    .get(paths.exercise(id))
    .then(({ data }) => data);    

export const createCollection = ({ name, exercises }) => axios
    .post(paths.collections, {
        name,
        exercises,
    })
    .then(({ data }) => data);

export const saveCollection = ({ name, id, exercises, description }) => axios
    .put(paths.collection(id), {
        name,
        exercises: exercises.map(({ id }) => id),
        description,
    })
    .then(({ data }) => data);

export const listAllSets = () => axios
    .get(paths.collections)
    .then(({ data }) => data);    

export const getCollectionData = id => axios
    .get(paths.collection(id))
    .then(({ data }) => data);

export const getCollectionPDF = id => axios
    .get(paths.collectionPDF(id))
    .then(({ data }) => data);

export const goToSet = setId => {
    window.location.href = `${window.location.origin}/collections/${setId}`;
}

export const addExerciseToCollection = (collectionId, exercisesIdentifiers) => axios
    .patch(paths.collection(collectionId), {
        "exercises": exercisesIdentifiers
    })
    .then(({ data }) => data );


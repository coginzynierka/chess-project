import * as constants from '../constants';

export const requestCollectionData = id => ({
  type: constants.REQUEST_COLLECTION_DATA,
  payload: {
    id,
  },
});

export const setCollectionData = collection => ({
  type: constants.SET_COLLECTION_DATA,
  payload: {
    collection,
  },
});

export const showQuickSearch = () => ({
  type: constants.SHOW_QUICK_SEARCH,
});

export const hideQuickSearch = () => ({
  type: constants.HIDE_QUICK_SEARCH,
});

export const addItemToCollection = (collectionId) => ({
  type: constants.ADD_ITEM_TO_COLLECTION,
  payload: {
    collectionId,
  },
});

export const removeItemFromCollection = (exerciseId, collectionId) => ({
  type: constants.REMOVE_ITEM_FROM_COLLECTION,
  payload: {
    exerciseId,
    collectionId,
  },
});

export const getCollectionPDF = collectionId => ({
  type: constants.GET_COLLECTION_PDF,
  payload: {
    collectionId,
  },
});

export const changeCollectionName = collectionName => ({
  type: constants.CHANGE_COLLECTION_NAME,
  payload: {
    collectionName,
  },
});

export const changeCollectionDescription = collectionDescription => ({
  type: constants.CHANGE_COLLECTION_DESCRIPTION,
  payload: {
    collectionDescription,
  },
});

export const saveCollection = () => ({
  type: constants.SAVE_COLLECTION,
})

export const toggleSaveCollectionConfirmation = () => ({
  type: constants.TOGGLE_SAVE_COLLECTION_CONFIRMATION,
});


export const moveExercise = (exerciseId, direction) => ({
  type: constants.MOVE_EXERCISE,
  payload: {
    exerciseId,
    direction,
  },
});
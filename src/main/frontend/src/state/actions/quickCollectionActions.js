import * as constants from '../constants';

export const addToQuickCollection = id => ({
  type: constants.ADD_TO_QUICK_COLLECTION,
  payload: {
    id,
  },
});

export const removeFromQuickCollection = id => ({
  type: constants.REMOVE_FROM_QUICK_COLLECTION,
  payload: {
    id,
  },
});

export const createNewQuickCollection = ids => ({
  type: constants.CREATE_QUICK_COLLECTION,
  payload: {
    ids,
  },
});

export const changeQuickCollectionName = name => ({
  type: constants.CHANGE_QUICK_COLLECTION_NAME,
  payload: {
    name,
  },
});

export const clearQuickCollection = () => ({
  type: constants.CLEAR_QUICK_COLLECTION,
});

export const addQuickCollectionToExistingCollection = collectionId => ({
  type: constants.ADD_QUICK_COLLECTION_TO_EXISTING_COLLECTION,
  payload: { collectionId }
})
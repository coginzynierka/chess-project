import * as constants from '../constants';

export const updateFenPosition = fenPosition => ({
  type: constants.UPDATE_FEN_INPUT,
  payload: {
    fenPosition,
  },
});

export const updateDraftFenPosition = fenPosition => ({
  type: constants.UPDATE_DRAFT_FEN_POSITION,
  payload: {
    fenPosition,
  },
});

export const updateExerciseName = name => ({
  type: constants.UPDATE_EXERCISE_NAME,
  payload: {
    name,
  },
});

export const updateExerciseDescription = description => ({
  type: constants.UPDATE_EXERCISE_DESCRIPTION,
  payload: {
    description,
  },
});

export const updateExerciseSolution = solution => ({
  type: constants.UPDATE_EXERCISE_SOLUTION,
  payload: {
    solution,
  },
});

export const addExerciseTag = tag => ({
  type: constants.ADD_EXERCISE_TAG,
});

export const createExercise = () => ({
  type: constants.CREATE_EXERCISE,
});

export const updateExercise = exerciseId => ({
  type: constants.SAVE_EXERCISE,
  payload: {
    exerciseId,
  },
});

export const setExerciseData = payload => ({
  type: constants.SET_EXERCISE_DATA,
  payload,
});

export const updateExercises = exercises => ({
  type: constants.UPDATE_EXERCISES,
  payload: {
    exercises,
  },
});

export const getExerciseData = id => ({
  type: constants.GET_EXERCISE_DATA,
  payload: {
    id,
  },
});

export const updateExerciseDifficulty = difficulty => ({
  type: constants.UPDATE_EXERCISE_DIFFICULTY,
  payload: {
    difficulty,
  },
});

export const updateExerciseNextMove = nextMoveColor => ({
  type: constants.UPDATE_EXERCISE_NEXT_MOVE,
  payload: {
    nextMoveColor,
  },
});

export const movePiece = (piece, position) => ({
  type: constants.MOVE_PIECE,
  payload: {
    piece,
    position,
  },
});

export const removePiece = position => ({
  type: constants.REMOVE_PIECE,
  payload: {
    position,
  },
});

export const toggleIsSavingExercise = () => ({
  type: constants.TOGGLE_IS_SAVING_EXERCISE,
});

export const swapItemInCollection = (exerciseId, collectionId) => ({
  type: constants.SWAP_ITEM_IN_COLLECTION,
  payload: {
    exerciseId,
    collectionId,
  },
});

export const resetBoadState = () => ({
  type: constants.RESET_BOARD_STATE,
})

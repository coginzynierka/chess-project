import * as constants from '../constants';

export const removeCollection = () => ({
  type: constants.REMOVE_COLLECTION,
});

export const updateCollectionName = name => ({
  type: constants.UPDATE_COLLECTION_NAME,
  payload: {
    name,
  },
});

export const clearCollection = () => ({
  type: constants.CLEAR_COLLECTION,
});

export const requestAllCollections = () => ({
  type: constants.REQUEST_ALL_COLECITONS,
});

export const setSetsData = sets => ({
  type: constants.SET_COLLECTIONS_DATA,
  payload: {
    sets,
  },
});

export const requestAllExercises = () => ({
  type: constants.REQUEST_ALL_EXERCISES,
});

export const createCollection = () => ({
  type: constants.CREATE_COLLECTION
});

export const filterExercises = ({ searchPhrase, difficultyFilters }) => ({
  type: constants.FILTER_EXERCISES,
  payload: {
    searchPhrase,
    difficultyFilters
  }
});

import * as constants from '../constants';

const initialState = {
  exercises: [],
  filteredExercises: [],
  collections: [],
  searchPhrase: '',
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case constants.UPDATE_EXERCISES:
      return {
        ...state,
        exercises: payload.exercises,
        filteredExercises: payload.exercises,
      };

    case constants.SET_COLLECTIONS_DATA:
      return {
        ...state,
        collections: payload.sets,
      };

    case constants.FILTER_EXERCISES: {

      return {
        ...state,
        filteredExercises: state.exercises.filter(({ name, description, difficulty }) =>
            (name.toLowerCase().includes(payload.searchPhrase.toLowerCase()) || description.toLowerCase().includes(payload.searchPhrase.toLowerCase())) &&
            ((payload.difficultyFilters.length === 0 ? [1,2,3,4,5] : payload.difficultyFilters).includes(difficulty))
        ),
        searchPhrase: payload.searchPhrase
        }
      }

    default:
      return state;
  }
};

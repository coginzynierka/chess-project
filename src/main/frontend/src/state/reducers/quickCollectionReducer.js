import * as constants from '../constants';

const initialState = {
  exercises: [],
  name: '',
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case constants.ADD_TO_QUICK_COLLECTION: {
      return {
        ...state,
        exercises: state.exercises.concat(payload.id),
      };
    }

    case constants.REMOVE_FROM_QUICK_COLLECTION: {
      return {
        ...state,
        exercises: state.exercises.filter(id => id !== payload.id),
      };
    }

    case constants.CHANGE_QUICK_COLLECTION_NAME: {
      return {
        ...state,
        name: payload.name,
      };
    }

    case constants.CLEAR_QUICK_COLLECTION: {
        return initialState;
    }

    default:
      return state;
  }
};

import * as constants from '../constants';
import {
  getBoardPositionsFromFEN,
  initialFenPosition,
  saveBoardPositionToFen,
  emptyBoardFen,
  getToMoveFromFen,
  WHITE_TO_MOVE
} from '../helpers/helpers';

const initialState = {
  boardPositions: getBoardPositionsFromFEN(initialFenPosition),
  fenPosition: initialFenPosition,
  draftFenPosition: initialFenPosition,
  exerciseName: '',
  exerciseDescription: '',
  exerciseSolution: '',
  difficulty: '3',
  nextMoveColour: WHITE_TO_MOVE,
  tags: [],
  isSavingExercise: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case constants.UPDATE_FEN_INPUT:
      return {
        ...state,
        boardPositions: payload.fenPosition
          ? getBoardPositionsFromFEN(payload.fenPosition)
          : getBoardPositionsFromFEN(emptyBoardFen),
        fenPosition: payload.fenPosition,
        draftFenPosition: payload.fenPosition,
      };

    case constants.UPDATE_DRAFT_FEN_POSITION:
      return {
        ...state,
        draftFenPosition: payload.fenPosition,
      };

    case constants.UPDATE_EXERCISE_DESCRIPTION:
      return {
        ...state,
        exerciseDescription: payload.description,
      };

    case constants.UPDATE_EXERCISE_SOLUTION:
      return {
        ...state,
        exerciseSolution: payload.solution,
      };

    case constants.UPDATE_EXERCISE_NAME:
      return {
        ...state,
        exerciseName: payload.name,
      };

    case constants.GET_EXERCISE_DATA:
      return {
        ...state,
        isFetching: true,
        exerciseId: payload.id,
      };

    case constants.SET_EXERCISE_DATA:
      return {
        ...state,
        boardPositions: getBoardPositionsFromFEN(payload.fen),
        fenPosition: payload.fen,
        draftFenPosition: payload.fen,
        exerciseName: payload.name,
        nextMoveColour: getToMoveFromFen(payload.fen),
        exerciseDescription: payload.description,
        exerciseSolution: payload.solution,
        difficulty: payload.difficulty || 1,
        isFetching: false,
        exerciseId: payload.id,
      };

    case constants.UPDATE_EXERCISE_DIFFICULTY:
      return {
        ...state,
        difficulty: parseInt(payload.difficulty),
      };

    case constants.UPDATE_EXERCISE_NEXT_MOVE:
      return {
        ...state,
        nextMoveColour: payload.nextMoveColor,
        fenPosition: saveBoardPositionToFen(state.boardPositions, payload.nextMoveColor),
        draftFenPosition: saveBoardPositionToFen(state.boardPositions, payload.nextMoveColor),
      };

    case constants.MOVE_PIECE: {
      const newBoardPositions = Array.from(state.boardPositions);

      newBoardPositions[payload.position.rowIndex][
        payload.position.fieldIndex
      ] = payload.piece;

      return {
        ...state,
        boardPositions: newBoardPositions,
        fenPosition: saveBoardPositionToFen(newBoardPositions, state.nextMoveColour),
        draftFenPosition: saveBoardPositionToFen(newBoardPositions, state.nextMoveColour),
        lastMovedPiecePosition: {
          row: payload.position.rowIndex,
          column: payload.position.fieldIndex,
        },
      };
    }

    case constants.REMOVE_PIECE: {
      const newBoardPositions = Array.from(state.boardPositions);

      if (
        state.lastMovedPiecePosition.row === payload.position.rowIndex &&
        state.lastMovedPiecePosition.column === payload.position.rowIndex
      ) {
        return {
          ...state,
          fenPosition: saveBoardPositionToFen(newBoardPositions, state.nextMoveColour),
          draftFenPosition: saveBoardPositionToFen(newBoardPositions, state.nextMoveColour),
          lastMovedPiecePosition: {},
        };
      }

      newBoardPositions[payload.position.rowIndex][
        payload.position.fieldIndex
      ] = null;

      return {
        ...state,
        fenPosition: saveBoardPositionToFen(newBoardPositions, state.nextMoveColour),
        draftFenPosition: saveBoardPositionToFen(newBoardPositions, state.nextMoveColour),
        boardPositions: newBoardPositions,
      };
    }

    case constants.TOGGLE_IS_SAVING_EXERCISE: {
      return {
        ...state,
        isSavingExercise: !state.isSavingExercise
      }
    }

    case constants.RESET_BOARD_STATE: {
      return initialState;
    }

    default:
      return state;
  }
};

import * as constants from '../constants';

const initialState = {
  id: '',
  exercises: [],
  name: '',
  isSearchOpen: false,
  description: '',
  isConfirmationMessageDisplayed: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case constants.SET_COLLECTION_DATA:
      return {
        ...state,
        ...payload.collection,
      };

    case constants.TOGGLE_SAVE_COLLECTION_CONFIRMATION:
      return {
        ...state,
        isConfirmationMessageDisplayed: !state.isConfirmationMessageDisplayed,
      };

    case constants.REMOVE_ITEM_FROM_COLLECTION:
      return {
        ...state,
        collection: state.collection.filter(el => el !== payload.id),
      };

    case constants.UPDATE_COLLECTION_NAME:
      return {
        ...state,
        collectionName: payload.name,
      };

    case constants.CLEAR_COLLECTION:
      return {
        ...state,
        collectionName: initialState.collectionName,
        collection: initialState.collection,
      };

    case constants.SHOW_QUICK_SEARCH:
      return {
        ...state,
        isSearchOpen: true,
      };

    case constants.HIDE_QUICK_SEARCH:
      return {
        ...state,
        isSearchOpen: false,
      };

    case constants.CHANGE_COLLECTION_NAME:
      return {
        ...state,
        name: payload.collectionName,
      };

    case constants.CHANGE_COLLECTION_DESCRIPTION:
      return {
        ...state,
        description: payload.collectionDescription,
      };

    case constants.MOVE_EXERCISE: {
      const exerciseIndex = state.exercises.findIndex(
        exercise => exercise.id === payload.exerciseId,
      );

      const newExerciseSort = [...state.exercises];

      newExerciseSort[exerciseIndex] =
        payload.direction === constants.DIRECTION.LEFT
          ? state.exercises[exerciseIndex - 1]
          : state.exercises[exerciseIndex + 1];

      newExerciseSort[
        payload.direction === constants.DIRECTION.LEFT
          ? exerciseIndex - 1
          : exerciseIndex + 1
      ] = state.exercises[exerciseIndex];

      return {
        ...state,
        exercises: newExerciseSort,
      };
    }

    default:
      return state;
  }
};

const chessBoardWidth = 8;
const chessBoardHeight = 8;

export const WHITE_TO_MOVE = 'WHITE';
export const BLACK_TO_MOVE = 'BLACK';

export const getToMoveFromFen = FEN => {
    if (!FEN || FEN === '') {
        return 'WHITE';
    }
    const values = FEN.split(' ');
    const toMoveValue = values.length >= 2 ? values[1] : 'w';
    return toMoveValue === 'w' ? WHITE_TO_MOVE : BLACK_TO_MOVE;
};

export const getBoardPositionsFromFEN = FEN => {
  const [piecesPositions] = FEN.split(' ');
  const boardRows = piecesPositions.split('/').map(row =>
    row
      .split('')
      .map(val => {
        const parsedValue = parseInt(val);

        return parsedValue ? new Array(parsedValue).fill(1) : [val];
      })
      .flat(),
  );

  const boardGrid = new Array(chessBoardWidth)
    .fill(new Array(chessBoardHeight).fill('*'))
    .map((boardRow, rowIndex) =>
      boardRow.map((field, fieldIndex) => {
        const fenFieldDescription = boardRows[rowIndex][fieldIndex];
        const isFieldInvalid =
          Boolean(parseInt(fenFieldDescription)) ||
          typeof fenFieldDescription === 'undefined';

        return isFieldInvalid ? null : fenFieldDescription;
      }),
    );

  return boardGrid;
};

// TODO: Make tests for that
export const saveBoardPositionToFen = (boardState, colourMove) => {
  let emptyFieldCounter = 0;

  return boardState
    .map(boardRow => boardRow
      .flatMap(el => el === null
        ? 0
        : el
      ).join('')
    ).join('/')
    .split('')
    .map((el, index, element) => {
      if (el === '/' && emptyFieldCounter > 0) {
        const emptyFieldCounterCopy = emptyFieldCounter;
        emptyFieldCounter = 0;
        return `${emptyFieldCounterCopy}/`
      } else if (el === '0' && index !== element.length -1 ) {
        emptyFieldCounter = emptyFieldCounter +1;
        return ''
      } else if (el !== '0' && el !== '/' && emptyFieldCounter > 0) {
        const emptyFieldCounterCopy = emptyFieldCounter;
        emptyFieldCounter = 0;
        return `${emptyFieldCounterCopy}${el}`;
      } else if (index === element.length -1 && el === '0') {
        return emptyFieldCounter +1;
      }
      else return el;
    }
    ).join('')
    .concat(` ${colourMove[0].toLowerCase()} -KQkq 0 1`)
}

export const getExerciseShapeFromState = boardState => ({
  name: boardState.exerciseName,
  description: boardState.exerciseDescription,
  solution: boardState.exerciseSolution,
  fen: boardState.fenPosition,
  tags: [],
  difficulty: boardState.difficulty,
  toMove: boardState.nextMoveColor,
});

export const emptyBoardFen = '8/8/8/8/8/8/8/8 w KQkq - 0 1';

export const initialFenPosition = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

export const getLichessLink = fen => `https://lichess.org/editor/${fen}`

export const boardPositions = [
  [null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
  [null, null, null, 'k', null, null, null, null],
  ['R', null, null, null, null, 'p', null, 'P'],
  [null, null, 'p', 'B', null, null, 'r', null],
  [null, null, null, 'b', 'K', null, null, null],
  [null, 'P', null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null],
];

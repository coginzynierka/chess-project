import { boardPositions } from './helpers.mock';
import { getToMoveFromFen, saveBoardPositionToFen, initialFenPosition } from './helpers';

describe('Helpers', () => {
    // nie dziala :/
    // describe('saveBoardPositionToFen', () => {
    //     it('should return proper fen', () => {
    //         expect(saveBoardPositionToFen(boardPositions)).toEqual('8/8/3k4/R4p1P/2pB2r1/3bK3/1P6/')
    //     });
    // });

    describe('getToMoveFromFen', () => {
        it('should return value "WHITE" for initial fen', () => {
            expect(getToMoveFromFen(initialFenPosition)).toEqual('WHITE');
        });

        it('should return value "WHITE" for empty string', () => {
            expect(getToMoveFromFen(null)).toEqual('WHITE');
            expect(getToMoveFromFen("")).toEqual('WHITE');
        });

        it('should return value "WHITE" for fen with no information about move', () => {
            expect(getToMoveFromFen("8/8/8/8/8/8/8/8")).toEqual('WHITE');
        });

        it('should return value "WHITE" for fen with white to move set', () => {
            expect(getToMoveFromFen('8/8/3k4/R4p1P/2pB2r1/3bK3/P7/8 w - - 2 49')).toEqual('WHITE');
            expect(getToMoveFromFen('8/8/3k4/R4p1P/2pB2r1/3bK3/P7/8 w')).toEqual('WHITE');
        });

        it('should return value "BLACK" for fen with black to move set', () => {
            expect(getToMoveFromFen('8/8/3k4/R4p1P/2pB2r1/3bK3/P7/8 b - - 2 49')).toEqual('BLACK');
            expect(getToMoveFromFen('8/8/3k4/R4p1P/2pB2r1/3bK3/P7/8 b')).toEqual('BLACK');
        });
    });
});

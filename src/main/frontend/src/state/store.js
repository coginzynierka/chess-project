import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import board from './reducers/boardReducer';
import search from './reducers/searchReducer';
import collection from './reducers/collectionReducer';
import quickCollection from './reducers/quickCollectionReducer';
import { rootSaga } from './sagas/saga';

const sagaMiddleware = createSagaMiddleware();

export default createStore(
  combineReducers({ board, search, collection, quickCollection}),
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__(),
  ),
);

sagaMiddleware.run(rootSaga)
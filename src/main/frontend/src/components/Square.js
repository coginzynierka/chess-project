import React from 'react'

import { useDrop } from 'react-dnd';

const getFieldColourClass = (rowIndex: Number, fieldIndex: Number) => {
    const isRowEven = rowIndex % 2;
    const isFieldEven = fieldIndex % 2;
  
    if ((isRowEven + isFieldEven) % 2 === 0) {
      return 'board-piece board-piece-light';
    } else {
      return 'board-piece board-piece-dark';
    }
  };

export const Square = ({ rowIndex, fieldIndex, children, onPieceMove }) => {
  
    const [, drop] = useDrop({
      accept: 'piece',
      drop: (item) => onPieceMove(item.piece, { rowIndex, fieldIndex }),
    });
  
    return (
      <div ref={drop} className={getFieldColourClass(rowIndex, fieldIndex)}>
        { children }
      </div>
    );
  };


import React from 'react';
import { useDrag, DragPreviewImage } from 'react-dnd';

import blackBishop from '../assets/black_bishop.svg';
import blackKnight from '../assets/black_knight.svg';
import blackRook from '../assets/black_rook.svg';
import blackPawn from '../assets/black_pawn.svg';
import blackKing from '../assets/black_king.svg';
import blackQueen from '../assets/black_queen.svg';

import whiteBishop from '../assets/white_bishop.svg';
import whiteKnight from '../assets/white_knight.svg';
import whiteRook from '../assets/white_rook.svg';
import whitePawn from '../assets/white_pawn.svg';
import whiteKing from '../assets/white_king.svg';
import whiteQueen from '../assets/white_queen.svg';

const pieceMap = {
  R: whiteRook,
  B: whiteBishop,
  N: whiteKnight,
  P: whitePawn,
  K: whiteKing,
  Q: whiteQueen,
  r: blackRook,
  b: blackBishop,
  n: blackKnight,
  p: blackPawn,
  k: blackKing,
  q: blackQueen,
};

export const Piece = ({ piece = null, onDragEnd = null }) => {
  const [{ isDragging }, drag, preview] = useDrag({
    item: {
      type: 'piece',
      piece,
    },
    end: onDragEnd,
  });

  return piece ? (
    <>
    <DragPreviewImage src={pieceMap[piece]} connect={preview}/>
    <img
      className={isDragging ? 'is-active' : ''}
      alt="piece"
      ref={drag}
      src={pieceMap[piece]}
    />
    </>
  ) : null;
};

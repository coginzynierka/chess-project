import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Link, Route } from 'react-router-dom';

import './App.css';
import store from './state/store';

import Board from './views/board/Board';
import ExerciseSearch from './views/exerciseSearch/ExerciseSearch';
import CollectionSearch from './views/collectionSearch/CollectionSearch';
import Collection from './views/collection/Collection';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider store={store}>
          <div className="navigation">
            <Link to="/board/">Stworz nowe zadanie --</Link>
            <Link to="/exercises/"> Zadania --</Link>
            <Link to="/collections/"> Zbiory</Link>
          </div>
          <div className="App">
            <Route path="/" exact component={Board} />
            <Route path="/board" exact component={Board} />
            <Route path="/board/:id" component={Board} />
            <Route path="/exercises" component={ExerciseSearch} />
            <Route path="/new-exercise/:collectionId" 
              render={props => (
                <Board {...props} isInCollectionContext={true}/>
              )}/>
            <Route path="/collections/:collectionId/change-exercise/:id" 
              render={props => (
                <Board {...props} isInCollectionContext={true} isInEditContext={true}/>
              )}
            />
            <Route
              path="/collections/:id/exercises"
              render={props => (
                <ExerciseSearch {...props} isInCollectionContext={true} />
              )}
            />
            <Route exact path="/collections/:id" component={Collection} />
            <Route path="/collections" exact component={CollectionSearch} />
          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;


import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { requestAllCollections } from '../../state/actions/searchActions';
import { Link } from 'react-router-dom';
import { paths } from '../../config';

const Sets = ({ actions, collections }) => {
  useEffect(() => { actions.requestAllCollections() }, []);

  return (
    <React.Fragment>
      <div className="search-sets">
        {collections.map(({ name, tags, id, exercises }) => (
          <div key={id} className="search-item">
            <Link to={`/collections/${id}`}>
              <div className="search-sets-preview">
              {
                exercises.slice(0, 4).map(
                  exercise => <img key={exercise.id} src={paths.image(exercise.id)}></img>
                )
              }
              </div>
              <h3>{name || 'nowa kolekcja'}</h3>
            </Link>
          </div>
        ))}
      </div>
    </React.Fragment>
  );
};

const mapStateToProps = ({ search }) => ({
    collections: search.collections
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      requestAllCollections,
    },
    dispatch,
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Sets);

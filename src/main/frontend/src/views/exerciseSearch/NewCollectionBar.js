import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { changeQuickCollectionName, createNewQuickCollection } from '../../state/actions/quickCollectionActions';

export const NewCollectionBar = () => {
  const dispatch = useDispatch();
  const quickCollection = useSelector(state => state.quickCollection);

  const actions = {
    changeQuickCollectionName: value => dispatch(changeQuickCollectionName(value)),
    createNewQuickCollection: () => dispatch(createNewQuickCollection())
  };

  return (
    <div
      className={`search-toolbar ${
        quickCollection.exercises.length ? 'is-active' : ''
      }`}
    >
      {quickCollection.exercises.length === 1 && <p>Zaznaczono 1 zadanie</p>}
      {quickCollection.exercises.length % 10 > 1 &&
        quickCollection.exercises.length % 10 <= 4 && (
          <p>Zaznaczono {quickCollection.exercises.length} zadania</p>
        )}
      {quickCollection.exercises.length % 10 > 4 && (
        <p>Zaznaczono {quickCollection.exercises.length} zadań</p>
      )}
      <input
        placeholder="Nazwa zbioru"
        onBlur={({ target }) => actions.changeQuickCollectionName(target.value)}
      />
      <button
        className="save-button"
        onClick={actions.createNewQuickCollection}
      >
        Utwórz zbiór
      </button>
    </div>
  );
};

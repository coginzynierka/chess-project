import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { requestAllExercises } from '../../state/actions/searchActions';
import {
  addToQuickCollection,
  removeFromQuickCollection,
} from '../../state/actions/quickCollectionActions';

import { paths } from '../../config';
import { ExerciseSearchFilterBar } from './ExerciseSearchFilterBar';
import { NewCollectionBar } from './NewCollectionBar';
import { AddToCollectionBar } from './AddToCollectionBar';

const ExerciseSearch = ({
  actions,
  exercises,
  collections,
  quickCollection,
  match,
  isInCollectionContext = false,
}) => {
  useEffect(() => {
    actions.requestAllExercises();
  }, []);

  return (
    <React.Fragment>
      <ExerciseSearchFilterBar />
      <div className="search-exercises">
        {exercises.map(({ id, name, difficulty }) => (
          <div key={id} className="search-item">
            <button
              className={`search-select-box ${
                quickCollection.exercises.includes(id) ? 'is-selected' : ''
              }`}
              onClick={() =>
                quickCollection.exercises.includes(id)
                  ? actions.removeFromQuickCollection(id)
                  : actions.addToQuickCollection(id)
              }
            >
              <div className="search-select-box-inner"></div>
            </button>
            <Link to={`/board/${id}`}>
              <img src={paths.image(id)} />
            </Link>
            <div className="exercise-description">
              <Link to={`/board/${id}`}>
                <h3>{name} ({difficulty + 1})</h3>
              </Link>
            </div>
          </div>
        ))}
      </div>
      <Link to="/board" className="search-new-exercise">
        <button className="save-button" type="button">
          Nowe zadanie
        </button>
      </Link>
      { isInCollectionContext 
        ? <AddToCollectionBar collectionId={match.params.id}/>
        : <NewCollectionBar /> 
      }
    </React.Fragment>
  );
};

const mapStateToProps = ({ search, quickCollection }) => ({
  exercises: search.filteredExercises,
  collections: search.collections,
  quickCollection: quickCollection,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      requestAllExercises,
      addToQuickCollection,
      removeFromQuickCollection,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExerciseSearch);

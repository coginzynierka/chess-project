import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { addQuickCollectionToExistingCollection } from '../../state/actions/quickCollectionActions';

export const AddToCollectionBar = ({ collectionId }) => {
  const dispatch = useDispatch();
  const quickCollection = useSelector(state => state.quickCollection);

  const actions = {
    addQuickCollectionToCollection: () => dispatch(addQuickCollectionToExistingCollection(collectionId)),
  };

  return (
    <div
      className={`search-toolbar ${
        quickCollection.exercises.length ? 'is-active' : ''
      }`}
    >
      {quickCollection.exercises.length === 1 && <p>Zaznaczono 1 zadanie</p>}
      {quickCollection.exercises.length % 10 > 1 &&
        quickCollection.exercises.length % 10 <= 4 && (
          <p>Zaznaczono {quickCollection.exercises.length} zadania</p>
        )}
      {quickCollection.exercises.length % 10 > 4 && (
        <p>Zaznaczono {quickCollection.exercises.length} zadań</p>
      )}
      <button
        className="save-button"
        onClick={actions.addQuickCollectionToCollection}
      >
        Dodaj do zbioru
      </button>
    </div>
  );
};

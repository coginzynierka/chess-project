import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import search from '../../assets/search.svg';
import cross from '../../assets/cross.png';
import { filterExercises } from '../../state/actions/searchActions';

export const ExerciseSearchFilterBar = () => {
  const dispatch = useDispatch();
  const haveSearchPhrase = useSelector(
    state => state.search.searchPhrase.length > 0,
  );
  const searchPhrase = useSelector(
    state => state.search.searchPhrase
  );
  const [textValue, setTextValue] = useState(searchPhrase);
  const [difficultyFilters, setDifficultyFilters] = useState([]);

  useEffect(() => {
    dispatch(filterExercises({ searchPhrase: textValue, difficultyFilters }));
  }, [textValue, difficultyFilters]);

  const handleTextChange = ({ target: { value }}) => setTextValue(value);
  const updateDifficulty = (value) => {
    if (difficultyFilters.includes(value)) {
      setDifficultyFilters(difficultyFilters.filter(filter => filter !== value));
    } else {
      setDifficultyFilters([...difficultyFilters, value]);
    }
  };

  return (
    <div className="exercise-filters">
      <div className="text-search">
        <input
          type="text"
          className="text-search-input"
          placeholder="Wyszukaj"
          value={searchPhrase}
          onChange={handleTextChange}
        />
        {!haveSearchPhrase && <img className="search-loop" alt="search-loop" src={search} />}
        {haveSearchPhrase && (
          <img
            className="search-cross"
            alt="search-cross"
            onClick={() => setTextValue('')}
            src={cross}
          />
        )}
      </div>
      <div className="difficulty-search">
        <div className="checkboxes">
          <input type="checkbox" onChange={() => updateDifficulty(0)} placeholder={1} />
          <input type="checkbox" onChange={() => updateDifficulty(1)} placeholder={2} />
          <input type="checkbox" onChange={() => updateDifficulty(2)} placeholder={3} />
          <input type="checkbox" onChange={() => updateDifficulty(3)} placeholder={4} />
          <input type="checkbox" onChange={() => updateDifficulty(4)} placeholder={5} />
        </div>
        <div className="name">
          Poziom trudności (1-5)
        </div>
      </div>
    </div>
  );
};

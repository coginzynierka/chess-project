import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ChessBoard } from './ChessBoard';
import { SaveButton } from './SaveButton';

import {
  updateFenPosition,
  updateDraftFenPosition,
  updateExerciseDescription,
  updateExerciseName,
  updateExerciseDifficulty,
  updateExerciseNextMove,
  updateExercise,
  createExercise,
  getExerciseData,
  movePiece,
  removePiece,
  updateExerciseSolution,
  resetBoadState
} from '../../state/actions/boardActions';

import {
  initialFenPosition,
  emptyBoardFen,
  BLACK_TO_MOVE,
  WHITE_TO_MOVE,
} from '../../state/helpers/helpers';

const Board = ({
  board,
  actions,
  match,
  isInEditContext,
  isInCollectionContext,
}) => {
  useEffect(() => {
    match.params.id && actions.getExerciseData(match.params.id);

    return () => actions.resetBoadState(emptyBoardFen);
  }, [match.params.id]);

  return (
    <div>
      <div
        className={`board-wrapper ${
          board.nextMoveColour === BLACK_TO_MOVE ? 'board-reversed' : ''
        }`}
      >
        <ChessBoard
          onPieceDragEnd={actions.removePiece}
          onPieceMove={actions.movePiece}
          onBoardReset={() => {
            actions.updateFenPosition(initialFenPosition);
            actions.updateExerciseNextMove(WHITE_TO_MOVE);
          }}
          onBoardClearance={() => actions.updateFenPosition(emptyBoardFen)}
          boardPositions={board.boardPositions}
          fenPosition={board.fenPosition}
        />
        <div className="board-controls">
          <label>
            FEN
            <input
              value={board.draftFenPosition}
              onChange={({ target }) =>
                actions.updateDraftFenPosition(target.value)
              }
              onBlur={({ target }) => actions.updateFenPosition(target.value)}
            />
          </label>
          <select
            value={board.nextMoveColour || WHITE_TO_MOVE}
            onChange={({ target }) =>
              actions.updateExerciseNextMove(target.value)
            }
          >
            <option value={WHITE_TO_MOVE}>Ruch bialych</option>
            <option value={BLACK_TO_MOVE}>Ruch czarnych</option>
          </select>
          <label>
            Poziom trudnosci
            <select
              value={board.difficulty || '0'}
              onChange={({ target }) =>
                actions.updateExerciseDifficulty(target.value)
              }
            >
              <option value="0">1</option>
              <option value="1">2</option>
              <option value="2">3</option>
              <option value="3">4</option>
              <option value="4">5</option>
            </select>
          </label>
          <label>
            Nazwa zadania
            <input
              value={board.exerciseName}
              onChange={({ target }) => actions.updateExerciseName(target.value)}
            />
          </label>
          <label>
            Opis zadania
            <input
              value={board.exerciseDescription}
              onChange={({ target }) =>
                actions.updateExerciseDescription(target.value)
              }
            />
          </label>
          <label>
            Rozwiązanie zadania
            <input
              value={board.exerciseSolution}
              onChange={({ target }) =>
                actions.updateExerciseSolution(target.value)
              }
            />
          </label>
          <SaveButton
            isInCollectionContext={isInCollectionContext}
            isInEditContext={isInEditContext}
            board={board}
            URLparams={match.params}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = ({ board }) => ({
  board,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      updateFenPosition,
      updateDraftFenPosition,
      updateExerciseDescription,
      updateExerciseSolution,
      updateExerciseName,
      updateExerciseDifficulty,
      updateExerciseNextMove,
      createExercise,
      updateExercise,
      getExerciseData,
      movePiece,
      removePiece,
      resetBoadState,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Board);

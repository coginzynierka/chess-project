import React from 'react';
import { useDispatch } from 'react-redux';

import {
  updateExercise,
  createExercise,
  swapItemInCollection,
} from '../../state/actions/boardActions';
import { addItemToCollection } from '../../state/actions/collectionActions';

const getSaveButtonText = ({ collectionId, id }, isSavingExercise, isInEditContext) => {
  if (isSavingExercise) {
    return '...';
  } else if (collectionId && !isInEditContext) {
    return 'Dodaj do zbioru';
  } else if (id) {
    return 'Zaktualizuj zadanie';
  } else {
    return 'Zapisz zadanie';
  }
};

const getActionToPerform = ({ collectionId, id }, actions, isInEditContext) => {
  if (collectionId && !isInEditContext) {
    return actions.addItemToCollection(collectionId);
  } else if (id) {
    return actions.updateExercise(id);
  } else {
    return actions.createExercise();
  }
};

export const SaveButton = ({ board, URLparams, isInEditContext }) => {
  const dispatch = useDispatch();

  const actions = {
    updateExercise: id => dispatch(updateExercise(id)),
    createExercise: () => dispatch(createExercise()),
    addItemToCollection: id => dispatch(addItemToCollection(id)),
    swapItemInCollection: ({ id, collectionId }) => dispatch(swapItemInCollection(id, collectionId))
  };

  return (
    <>
      <button
        className={`save-button ${board.isSavingExercise ? 'is-saving' : ''}`}
        type="button"
        onClick={() => getActionToPerform(URLparams, actions, isInEditContext)}
      >
        {getSaveButtonText(URLparams, board.isSavingExercise, isInEditContext)}
      </button>
      {isInEditContext && <button className="save-button" onClick={() => actions.swapItemInCollection(URLparams)}>Zmien w zbiorze</button>}
    </>
  );
};

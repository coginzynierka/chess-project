import React, { useRef } from 'react';
import { DndProvider, createDndContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import { Piece } from '../../components/Piece';
import { Square } from '../../components/Square';
import { getLichessLink } from '../../state/helpers/helpers';

export const ChessBoard = ({
  boardPositions,
  fenPosition,
  onPieceMove,
  onPieceDragEnd,
  onBoardReset,
  onBoardClearance,
}) => {
  const manager = useRef(createDndContext(HTML5Backend));

  return (
    <DndProvider manager={manager.current.dragDropManager}>
      <div className="board-outer-container">
        <button
          className="save-button "
          type="button"
          onClick={onBoardClearance}
        >
          Wyczysc szachownice
        </button>
        <button className="save-button " type="button" onClick={onBoardReset}>
          Zresetuj szachownice
        </button>
        <a target="_blank" className="lichess-link" rel="noopener noreferrer" href={getLichessLink(fenPosition)}>
            Otworz w lichess
          </a>
        <div className="board-container">
          <PiecesRow colour="black" />
          {boardPositions.map((boardRow, rowIndex) => (
            <div key={rowIndex} className="board-row">
              {boardRow.map((chessPiece, fieldIndex) => (
                <Square
                  key={fieldIndex}
                  rowIndex={rowIndex}
                  fieldIndex={fieldIndex}
                  onPieceMove={onPieceMove}
                >
                  <Piece
                    onDragEnd={() => onPieceDragEnd({ rowIndex, fieldIndex })}
                    piece={chessPiece}
                  />
                </Square>
              ))}
            </div>
          ))}
          <PiecesRow colour="white" />
        </div>
      </div>
    </DndProvider>
  );
};

export const PiecesRow = ({ colour }) => (
  <div className="board-container">
    <div className="board-row">
      <div className="board-piece">
        <Piece piece={colour === 'white' ? 'R' : 'r'} />
      </div>
      <div className="board-piece">
        <Piece piece={colour === 'white' ? 'B' : 'b'} />
      </div>
      <div className="board-piece">
        <Piece piece={colour === 'white' ? 'N' : 'n'} />
      </div>
      <div className="board-piece">
        <Piece piece={colour === 'white' ? 'P' : 'p'} />
      </div>
      <div className="board-piece">
        <Piece piece={colour === 'white' ? 'K' : 'k'} />
      </div>
      <div className="board-piece">
        <Piece piece={colour === 'white' ? 'Q' : 'q'} />
      </div>
    </div>
  </div>
);

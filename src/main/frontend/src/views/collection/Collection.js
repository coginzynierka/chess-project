import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  requestCollectionData,
  showQuickSearch,
  hideQuickSearch,
  addItemToCollection,
  getCollectionPDF,
  changeCollectionName,
  changeCollectionDescription,
  saveCollection,
  moveExercise,
} from '../../state/actions/collectionActions';

import { paths } from '../../config';

import arrowRight from '../../assets/arrow-right.png';
import { DIRECTION } from '../../state/constants';

const Collection = ({ actions, collection, exercises, match }) => {
  useEffect(() => {
    actions.requestCollectionData(match.params.id);
  }, false);

  return (
    <div>
      <h2>{collection.name}</h2>
      <h3>Exercises: </h3>
      <div className="collection-content">
        <div className="collection-exercises">
          {collection.exercises.map((exercise, index, array) => (
            <div className="collection-exercise">
              {index > 0 && (
                <img
                  className="edit-icon left"
                  onClick={() =>
                    actions.moveExercise(exercise.id, DIRECTION.LEFT)
                  }
                  src={arrowRight}
                  alt="move exercise left"
                />
              )}
              { index < array.length - 1 && 
              <img
                className="edit-icon right"
                onClick={() =>
                  actions.moveExercise(exercise.id, DIRECTION.RIGHT)
                }
                src={arrowRight}
                alt="move exercise right"
              />
              }
              <Link
                to={`/collections/${collection.id}/change-exercise/${exercise.id}`}
              >
                <img src={paths.image(exercise.id)} alt="exercise preview" />
              </Link>
            </div>
          ))}
        </div>
        <div className="collection-data">
          <label>
            Nazwa
            <input
              onBlur={({ target }) =>
                actions.changeCollectionName(target.value)
              }
              placeholder={collection.name}
            />
          </label>
          <label>
            Opis
            <input
              placeholder={collection.description}
              onBlur={({ target }) =>
                actions.changeCollectionDescription(target.value)
              }
            />
          </label>
          <button className="save-button" onClick={actions.saveCollection}>
            Zapisz zbior
          </button>
          {collection.isConfirmationMessageDisplayed && (
            <p>Zbior zostal zapisany</p>
          )}
          <a
            className="collection-pdf-link"
            href={paths.collectionPDF(collection.id)}
          >
            Pobierz
          </a>
        </div>
      </div>
      <div className="collection-bottom-bar">
        <Link
          className="add-exercises-button"
          to={`/collections/${collection.id}/exercises`}
        >
          Dodaj zadania
        </Link>
        <Link
          className="add-exercises-button"
          to={`/new-exercise/${collection.id}`}
        >
          Stworz nowe zadanie
        </Link>
      </div>
    </div>
  );
};

const mapStateToProps = ({ collection, search }) => ({
  collection,
  exercises: search.exercises,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      requestCollectionData,
      showQuickSearch,
      hideQuickSearch,
      addItemToCollection,
      getCollectionPDF,
      changeCollectionName,
      saveCollection,
      changeCollectionDescription,
      moveExercise,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Collection);

package pl.edu.amu.chessproject.infrastructure.file;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import pl.edu.amu.chessproject.domain.model.ChessBoard;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.port.ChessBoardRepository;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

@Component
@ConditionalOnProperty(prefix = "chessBoardRepository.file", name = "cacheEnabled", havingValue = "true")
public class FileChessBoardRepository implements ChessBoardRepository {

    private String path;

    public FileChessBoardRepository(@Value("${chessBoardRepository.file.path}") String path) {
        this.path = path;
    }

    @Override
    public ChessBoard save(ChessBoard chessBoard) {
        try {
            File file = new File(path + "\\" +
                    chessBoard.getSize().toString().toLowerCase() + "\\" +
                    chessBoard.getId() + "." + "png");
            ImageIO.write(chessBoard.getBoard(), "png", file);
            return chessBoard;
        } catch (IOException e) {
            e.printStackTrace(); //TODO - add logger
            return null;
        }
    }

    @Override
    public Optional<ChessBoard> findByIdAndSize(String id, ChessBoardSize size) {
        try {
            File file = new File(path + "\\" + size.toString().toLowerCase() + "\\" + id + ".png");
            if (file.exists()) {
                BufferedImage bufferedImage = ImageIO.read(file);
                return Optional.of(new ChessBoard(id, bufferedImage, size));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @PostConstruct
    private void init() {
        File boardsDirectory = new File(path);
        if (!boardsDirectory.exists()) {
            boardsDirectory.mkdir();
        }
        for (ChessBoardSize value : ChessBoardSize.values()) {
            File boardSizeDirectory = new File(boardsDirectory, value.toString().toLowerCase());
            if (!boardSizeDirectory.exists()) {
                boardSizeDirectory.mkdir();
            }
        }
    }
}

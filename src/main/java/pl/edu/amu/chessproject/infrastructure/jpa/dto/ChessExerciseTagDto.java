package pl.edu.amu.chessproject.infrastructure.jpa.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.edu.amu.chessproject.domain.model.ChessExerciseTag;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@Entity
@Table(name = "chess_exercise_tag")
@NoArgsConstructor
@AllArgsConstructor
public class ChessExerciseTagDto {

    public static ChessExerciseTagDto fromDomain(ChessExerciseTag domain) {
        return builder()
                .id(domain.getId())
                .name(domain.getName())
                .description(domain.getDescription())
                .build();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;

    public ChessExerciseTag toDomain() {
        return ChessExerciseTag.builder()
                .id(id)
                .name(name)
                .description(description)
                .build();
    }
}

package pl.edu.amu.chessproject.infrastructure.jpa.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.fen.Fen;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "chess_exercise")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChessExerciseDto {

    public static ChessExerciseDto fromDomain(ChessExercise domainObject) {
        return builder()
                .id(domainObject.getId())
                .name(domainObject.getName())
                .description(domainObject.getDescription())
                .solution(domainObject.getSolution())
                .tags(domainObject.getTags().stream().map(ChessExerciseTagDto::fromDomain).collect(Collectors.toList()))
                .fen(domainObject.getFen().getFen())
                .difficulty(domainObject.getDifficulty())
                .build();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private String solution;
    private String fen;
    @ManyToMany(cascade = {CascadeType.ALL})
    private List<ChessExerciseTagDto> tags;
    @Enumerated
    private ChessExercise.Difficulty difficulty;

    public ChessExercise toDomain() {
        return ChessExercise.builder()
                .id(id)
                .name(name)
                .description(description)
                .solution(solution)
                .fen(new Fen(fen))
                .tags(tags.stream().map(ChessExerciseTagDto::toDomain).collect(Collectors.toList()))
                .difficulty(difficulty)
                .build();
    }
}

package pl.edu.amu.chessproject.infrastructure.jpa.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "chess_exercise_set")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChessExerciseSetDto {

    public static ChessExerciseSetDto fromDomain(ChessExerciseSet domainObject) {
        return ChessExerciseSetDto.builder()
                .id(domainObject.getId())
                .name(domainObject.getName())
                .description(domainObject.getDescription())
                .exercises(domainObject.getExercises()
                        .stream()
                        .map(ChessExerciseDto::fromDomain)
                        .collect(Collectors.toList())
                )
                .build();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToMany(cascade = {CascadeType.ALL})
    private List<ChessExerciseDto> exercises;

    public ChessExerciseSet toDomain() {
        return ChessExerciseSet.builder()
                .id(id)
                .name(name)
                .description(description)
                .exercises(exercises.stream()
                        .map(ChessExerciseDto::toDomain)
                        .collect(Collectors.toList()))
                .build();
    }
}

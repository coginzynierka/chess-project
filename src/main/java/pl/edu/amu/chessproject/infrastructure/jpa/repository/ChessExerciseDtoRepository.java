package pl.edu.amu.chessproject.infrastructure.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseDto;

public interface ChessExerciseDtoRepository extends JpaRepository<ChessExerciseDto, Long> {
}

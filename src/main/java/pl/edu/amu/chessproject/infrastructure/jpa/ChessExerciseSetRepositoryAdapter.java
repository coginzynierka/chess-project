package pl.edu.amu.chessproject.infrastructure.jpa;

import org.springframework.stereotype.Component;
import pl.edu.amu.chessproject.domain.exceptions.InvalidResourceException;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;
import pl.edu.amu.chessproject.domain.port.ChessExerciseSetRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseDto;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseSetDto;
import pl.edu.amu.chessproject.infrastructure.jpa.repository.ChessExerciseDtoRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.repository.ChessExerciseSetDtoRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ChessExerciseSetRepositoryAdapter implements ChessExerciseSetRepository {
    private final ChessExerciseSetDtoRepository chessExerciseSetDtoRepository;
    private final ChessExerciseDtoRepository chessExerciseDtoRepository;

    public ChessExerciseSetRepositoryAdapter(ChessExerciseSetDtoRepository chessExerciseSetDtoRepository, ChessExerciseDtoRepository chessExerciseDtoRepository) {
        this.chessExerciseSetDtoRepository = chessExerciseSetDtoRepository;
        this.chessExerciseDtoRepository = chessExerciseDtoRepository;
    }

    @Override
    public Optional<ChessExerciseSet> findById(Long id) {
        return chessExerciseSetDtoRepository.findById(id)
                .map(ChessExerciseSetDto::toDomain);
    }

    @Override
    public List<ChessExerciseSet> findAll() {
        return chessExerciseSetDtoRepository.findAll()
                .stream()
                .map(ChessExerciseSetDto::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public ChessExerciseSet save(ChessExerciseSet chessExerciseSet) {
        if (chessExerciseSet.getExercises()
                .stream()
                .anyMatch(e -> e.getId() == null)) {
            throw new InvalidResourceException("Some chess exercises are not present in database");
        }

        List<ChessExerciseDto> exercises = findChessExercises(chessExerciseSet.getExercises());

        return chessExerciseSetDtoRepository.save(ChessExerciseSetDto.builder()
                .id(chessExerciseSet.getId())
                .name(chessExerciseSet.getName())
                .description(chessExerciseSet.getDescription())
                .exercises(exercises)
                .build())
                .toDomain();
    }

    private List<ChessExerciseDto> findChessExercises(List<ChessExercise> exercises) {
        return exercises.stream()
                .map(ChessExercise::getId)
                .map(id -> chessExerciseDtoRepository.findById(id)
                        .orElseThrow(() -> new InvalidResourceException("Chess exercise with id " + id + " does not exist"))
                )
                .collect(Collectors.toList());
    }
}

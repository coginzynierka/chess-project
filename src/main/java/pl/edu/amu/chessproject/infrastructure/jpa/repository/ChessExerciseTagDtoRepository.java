package pl.edu.amu.chessproject.infrastructure.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseTagDto;

public interface ChessExerciseTagDtoRepository extends JpaRepository<ChessExerciseTagDto, Long> {
}

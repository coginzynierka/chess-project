package pl.edu.amu.chessproject.infrastructure.file;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import pl.edu.amu.chessproject.domain.model.ChessBoard;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.port.ChessBoardRepository;

import java.util.Optional;

@Component
@Primary
@ConditionalOnProperty(prefix = "chessBoardRepository.file", name = "cacheEnabled", havingValue = "false")
public class DisabledCacheFileChessBoardRepository implements ChessBoardRepository {
    @Override
    public ChessBoard save(ChessBoard chessBoard) {
        return chessBoard;
    }

    @Override
    public Optional<ChessBoard> findByIdAndSize(String id, ChessBoardSize size) {
        return Optional.empty();
    }
}

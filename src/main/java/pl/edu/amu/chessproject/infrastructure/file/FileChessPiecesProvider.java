package pl.edu.amu.chessproject.infrastructure.file;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.model.Piece;
import pl.edu.amu.chessproject.domain.port.ChessPiecesProvider;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Component
public class FileChessPiecesProvider implements ChessPiecesProvider {


    @Override
    public BufferedImage getPieceImage(Piece piece, ChessBoardSize size) {
        try {
            Resource resource = new ClassPathResource("pieces/" + size.toString().toLowerCase() + "/" + piece.getFullName() + ".png");
            return ImageIO.read(resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace(); //TODO: introduce logger
        }
        return null;
    }
}

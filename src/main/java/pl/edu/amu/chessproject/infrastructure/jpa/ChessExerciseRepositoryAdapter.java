package pl.edu.amu.chessproject.infrastructure.jpa;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.amu.chessproject.domain.exceptions.InvalidResourceException;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.ChessExerciseTag;
import pl.edu.amu.chessproject.domain.port.ChessExerciseRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseDto;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseTagDto;
import pl.edu.amu.chessproject.infrastructure.jpa.repository.ChessExerciseDtoRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.repository.ChessExerciseTagDtoRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ChessExerciseRepositoryAdapter implements ChessExerciseRepository {
    private final ChessExerciseDtoRepository chessExerciseDtoRepository;
    private final ChessExerciseTagDtoRepository tagRepository;


    public ChessExerciseRepositoryAdapter(ChessExerciseDtoRepository chessExerciseDtoRepository, ChessExerciseTagDtoRepository tagRepository) {
        this.chessExerciseDtoRepository = chessExerciseDtoRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    public Optional<ChessExercise> findById(Long id) {
        return chessExerciseDtoRepository.findById(id)
                .map(ChessExerciseDto::toDomain);
    }

    @Override
    public List<ChessExercise> findAll() {
        return chessExerciseDtoRepository.findAll()
                .stream()
                .map(ChessExerciseDto::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ChessExercise save(ChessExercise chessExercise) {
        List<ChessExerciseTagDto> tags = findOrCreateTags(chessExercise.getTags());

        ChessExerciseDto chessExerciseDto = ChessExerciseDto.builder()
                .id(chessExercise.getId())
                .name(chessExercise.getName())
                .description(chessExercise.getDescription())
                .solution(chessExercise.getSolution())
                .fen(chessExercise.getFen().getFen())
                .tags(tags)
                .difficulty(chessExercise.getDifficulty())
                .build();
        return chessExerciseDtoRepository.save(chessExerciseDto).toDomain();
    }

    private List<ChessExerciseTagDto> findOrCreateTags(List<ChessExerciseTag> tags) {
        return tags.stream()
                .map(ChessExerciseTagDto::fromDomain)
                .map(tag -> {
                    Long tagId = tag.getId();
                    if (tagId == null) {
                        return tagRepository.save(tag);
                    } else {
                        return tagRepository.findById(tagId)
                                .orElseThrow(() -> new InvalidResourceException("Tag with id " + tagId + " does not exist"));
                    }
                })
                .collect(Collectors.toList());
    }

}

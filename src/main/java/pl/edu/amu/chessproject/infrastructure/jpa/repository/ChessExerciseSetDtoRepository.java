package pl.edu.amu.chessproject.infrastructure.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.amu.chessproject.infrastructure.jpa.dto.ChessExerciseSetDto;

public interface ChessExerciseSetDtoRepository extends JpaRepository<ChessExerciseSetDto, Long> {
}

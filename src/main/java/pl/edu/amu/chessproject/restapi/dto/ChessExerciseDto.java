package pl.edu.amu.chessproject.restapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.fen.Fen;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class ChessExerciseDto {
    public static ChessExerciseDto fromDomain(ChessExercise domainObject) {
        return builder()
                .id(domainObject.getId())
                .name(domainObject.getName())
                .description(domainObject.getDescription())
                .solution(domainObject.getSolution())
                .fen(domainObject.getFen().getFen())
                .difficulty(domainObject.getDifficulty())
                .tags(domainObject.getTags().stream().map(ChessExerciseTagDto::fromDomain).collect(Collectors.toList()))
                .build();
    }

    private Long id;
    private String name;
    private String description;
    private String solution;
    private String fen;
    private ChessExercise.Difficulty difficulty;
    private List<ChessExerciseTagDto> tags;

    @JsonCreator
    public ChessExerciseDto(
            @JsonProperty("id") Long id,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("solution") String solution,
            @JsonProperty("fen") String fen,
            @JsonProperty("difficulty") ChessExercise.Difficulty difficulty,
            @JsonProperty("tags") List<ChessExerciseTagDto> tags) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.solution = solution;
        this.fen = fen;
        this.difficulty = difficulty;
        this.tags = tags;
    }

    public ChessExercise toDomain() {
        return ChessExercise.builder()
                .id(id)
                .name(name)
                .description(description)
                .solution(solution)
                .fen(new Fen(fen))
                .difficulty(difficulty)
                .tags(tags != null ? tags.stream().map(ChessExerciseTagDto::toDomain).collect(Collectors.toList()) : null)
                .build();
    }
}

package pl.edu.amu.chessproject.restapi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.amu.chessproject.domain.ChessExerciseSetPdfRepresentationService;
import pl.edu.amu.chessproject.domain.ChessExerciseSetService;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSetRepresentationSizes;
import pl.edu.amu.chessproject.domain.model.DocumentWrapper;
import pl.edu.amu.chessproject.restapi.dto.ChessExerciseSetCreationDto;
import pl.edu.amu.chessproject.restapi.dto.ChessExerciseSetDto;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/chess-exercise-set")
public class ChessExerciseSetEndpoint {
    private final ChessExerciseSetService chessExerciseSetService;
    private final ChessExerciseSetPdfRepresentationService representationService;

    public ChessExerciseSetEndpoint(ChessExerciseSetService chessExerciseSetService, ChessExerciseSetPdfRepresentationService representationService) {
        this.chessExerciseSetService = chessExerciseSetService;
        this.representationService = representationService;
    }

    @GetMapping("{id}")
    public ChessExerciseSetDto findById(@PathVariable Long id) {
        return ChessExerciseSetDto.fromDomain(chessExerciseSetService.findById(id));
    }

    @GetMapping("{id}/pdf")
    public ResponseEntity<byte[]> findChessSetPdfById(@PathVariable Long id) {
        ChessExerciseSet set = chessExerciseSetService.findById(id);
        DocumentWrapper documentWrapper = representationService.createPdfDocumentFromSet(set, ChessExerciseSetRepresentationSizes.THREE_COLUMNS);

        return new ResponseEntity<>(documentWrapper.toByteArray(), createPdfHeaders(set), HttpStatus.OK);
    }

    @GetMapping
    public List<ChessExerciseSetDto> findAll() {
        return chessExerciseSetService.findAll()
                .stream()
                .map(ChessExerciseSetDto::fromDomain)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ChessExerciseSetDto save(@RequestBody ChessExerciseSetCreationDto dto) {
        return ChessExerciseSetDto.fromDomain(chessExerciseSetService.save(dto.toDomain()));
    }

    @PostMapping("/{setId}/exercises")
    public ChessExerciseSetDto addExerciseToSet(@PathVariable("setId") Long setId, @RequestBody AddExerciseDto addExercise) {
        return ChessExerciseSetDto.fromDomain(chessExerciseSetService.addExercise(setId, addExercise.id));
    }

    @PutMapping("/{setId}")
    public ChessExerciseSetDto updateExerciseSet(@PathVariable("setId") Long setId, @RequestBody ChessExerciseSetCreationDto dto) {
        return ChessExerciseSetDto.fromDomain(chessExerciseSetService.update(setId, dto.toDomain()));
    }

    @PatchMapping("/{setId}")
    public ChessExerciseSetDto patchExerciseSet(@PathVariable("setId") Long setId, @RequestBody ChessExerciseSetCreationDto dto) {
        return ChessExerciseSetDto.fromDomain(chessExerciseSetService.patch(setId, dto.toDomain()));
    }

    private HttpHeaders createPdfHeaders(ChessExerciseSet set) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = String.format("%s-%s.pdf", set.getName(), set.getId());
        headers.setContentDispositionFormData(filename, filename);
        return headers;
    }

    @Data
    private static class AddExerciseDto {
        private Long id;

        @JsonCreator
        public AddExerciseDto(@JsonProperty("id") Long id) {
            this.id = id;
        }
    }
}

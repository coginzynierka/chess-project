package pl.edu.amu.chessproject.restapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import pl.edu.amu.chessproject.domain.model.ChessExerciseTag;

@Data
@Builder
class ChessExerciseTagDto {
    static ChessExerciseTagDto fromDomain(ChessExerciseTag domain) {
        return builder()
                .id(domain.getId())
                .name(domain.getName())
                .description(domain.getDescription())
                .build();
    }

    private Long id;
    private String name;
    private String description;

    @JsonCreator
    public ChessExerciseTagDto(
            @JsonProperty("id") Long id,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    ChessExerciseTag toDomain() {
        return ChessExerciseTag.builder()
                .id(id)
                .name(name)
                .description(description)
                .build();
    }
}

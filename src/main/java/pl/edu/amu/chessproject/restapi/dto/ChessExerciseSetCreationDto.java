package pl.edu.amu.chessproject.restapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class ChessExerciseSetCreationDto {
    private final String name;
    private final String description;
    private final List<Long> exercises;

    @JsonCreator
    public ChessExerciseSetCreationDto(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty(value = "exercises") List<Long> exercises) {
        this.name = name;
        this.description = description;
        this.exercises = exercises;
    }

    public ChessExerciseSet toDomain() {
        return ChessExerciseSet.builder()
                .name(name)
                .description(description)
                .exercises(toExercisesList(exercises))
                .build();
    }

    private List<ChessExercise> toExercisesList(List<Long> exercises) {
        return Optional.of(exercises)
                .orElse(Collections.emptyList())
                .stream()
                .map(e -> ChessExercise.builder().id(e).build())
                .collect(Collectors.toList());
    }
}

package pl.edu.amu.chessproject.restapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.amu.chessproject.domain.ChessBoardService;
import pl.edu.amu.chessproject.domain.ChessExerciseService;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.restapi.dto.ChessExerciseDto;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/chess-exercise")
public class ChessExerciseEndpoint {
    private final ChessExerciseService chessExerciseService;
    private final ChessBoardService chessBoardService;

    public ChessExerciseEndpoint(ChessExerciseService chessExerciseService, ChessBoardService chessBoardService) {
        this.chessExerciseService = chessExerciseService;
        this.chessBoardService = chessBoardService;
    }

    @GetMapping
    public List<ChessExerciseDto> findAll() {
        return chessExerciseService.findAll()
                .stream()
                .map(ChessExerciseDto::fromDomain)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ChessExerciseDto findById(@PathVariable("id") Long id) {
        return ChessExerciseDto.fromDomain(chessExerciseService.findById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ChessExerciseDto createChessExercise(@RequestBody ChessExerciseDto dto) {
        return ChessExerciseDto.fromDomain(chessExerciseService.save(dto.toDomain()));
    }

    @PutMapping("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public ChessExerciseDto updateChessExercise(@PathVariable("id") Long id, @RequestBody ChessExerciseDto dto) {
        return ChessExerciseDto.fromDomain(chessExerciseService.update(id, dto.toDomain()));
    }

    @GetMapping(value = "/{id}/chess-board", produces = "image/png")
    public byte[] getImageForChessExercise(@PathVariable("id") Long chessExerciseId, final HttpServletResponse response) throws IOException {
        ChessExercise chessExercise = chessExerciseService.findById(chessExerciseId);
        BufferedImage board = chessBoardService.getOrCreateFromChessExercise(chessExercise, ChessBoardSize.MEDIUM)
                .getBoard();

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ImageIO.write(board, "png", bao);
        response.addHeader("Cache-Control", "public, max-age=0");

        return bao.toByteArray();
    }
}

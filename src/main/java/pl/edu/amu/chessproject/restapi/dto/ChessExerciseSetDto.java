package pl.edu.amu.chessproject.restapi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class ChessExerciseSetDto {
    public static ChessExerciseSetDto fromDomain(ChessExerciseSet domainObject) {
        return ChessExerciseSetDto.builder()
                .id(domainObject.getId())
                .name(domainObject.getName())
                .description(domainObject.getDescription())
                .exercises(domainObject.getExercises()
                        .stream()
                        .map(ChessExerciseDto::fromDomain)
                        .collect(Collectors.toList())
                )
                .build();
    }

    private Long id;
    private String name;
    private String description;
    private List<ChessExerciseDto> exercises;

    @JsonCreator
    public ChessExerciseSetDto(
            @JsonProperty("id") Long id,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("exercises") List<ChessExerciseDto> exercises) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.exercises = exercises;
    }

    public ChessExerciseSet toDomain() {
        return ChessExerciseSet.builder()
                .id(id)
                .name(name)
                .description(description)
                .exercises(exercises.stream()
                        .map(ChessExerciseDto::toDomain)
                        .collect(Collectors.toList()))
                .build();
    }
}

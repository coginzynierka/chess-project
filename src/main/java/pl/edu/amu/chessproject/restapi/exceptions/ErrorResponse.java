package pl.edu.amu.chessproject.restapi.exceptions;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class ErrorResponse {
    private String cause;
}

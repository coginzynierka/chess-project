package pl.edu.amu.chessproject.restapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.edu.amu.chessproject.domain.exceptions.InvalidResourceException;
import pl.edu.amu.chessproject.domain.exceptions.ResourceAlreadyExistsException;
import pl.edu.amu.chessproject.domain.exceptions.ResourceNotFoundException;

@RestControllerAdvice
public class ResourceExceptionsHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleResourceNotFoundException(ResourceNotFoundException e) {
        return ErrorResponse.builder()
                .cause(e.getMessage())
                .build();
    }

    @ExceptionHandler(InvalidResourceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidResourceException(InvalidResourceException e) {
        return ErrorResponse.builder()
                .cause(e.getMessage())
                .build();
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleResourceAlreadyExistsException(ResourceAlreadyExistsException e) {
        return ErrorResponse.builder()
                .cause(e.getMessage())
                .build();
    }
}

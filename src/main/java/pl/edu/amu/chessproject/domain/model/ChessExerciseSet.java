package pl.edu.amu.chessproject.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChessExerciseSet {
    private Long id;
    private String name;
    private String description;
    private List<ChessExercise> exercises;
}

package pl.edu.amu.chessproject.domain.model.fen;

public enum ToMove {
    WHITE, BLACK
}

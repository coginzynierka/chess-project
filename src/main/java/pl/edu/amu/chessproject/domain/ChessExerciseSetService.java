package pl.edu.amu.chessproject.domain;

import org.springframework.stereotype.Service;
import pl.edu.amu.chessproject.domain.exceptions.InvalidResourceException;
import pl.edu.amu.chessproject.domain.exceptions.ResourceAlreadyExistsException;
import pl.edu.amu.chessproject.domain.exceptions.ResourceNotFoundException;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;
import pl.edu.amu.chessproject.domain.port.ChessExerciseRepository;
import pl.edu.amu.chessproject.domain.port.ChessExerciseSetRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ChessExerciseSetService {
    private final ChessExerciseSetRepository chessExerciseSetRepository;
    private final ChessExerciseRepository chessExerciseRepository;

    public ChessExerciseSetService(
            ChessExerciseSetRepository chessExerciseSetRepository,
            ChessExerciseRepository chessExerciseRepository) {
        this.chessExerciseSetRepository = chessExerciseSetRepository;
        this.chessExerciseRepository = chessExerciseRepository;
    }

    public List<ChessExerciseSet> findAll() {
        return chessExerciseSetRepository.findAll();
    }

    public ChessExerciseSet findById(Long id) {
        return chessExerciseSetRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("chess exercise set with id " + id + " not found"));
    }

    public ChessExerciseSet save(ChessExerciseSet chessExerciseSet) {
        return chessExerciseSetRepository.save(chessExerciseSet);
    }

    public ChessExerciseSet addExercise(Long setId, Long exerciseId) {
        ChessExerciseSet set = chessExerciseSetRepository.findById(setId)
                .orElseThrow(() -> new ResourceNotFoundException("chess exercise set with id " + setId + " not found"));
        ChessExercise chessExercise = chessExerciseRepository.findById(exerciseId)
                .orElseThrow(() -> new ResourceNotFoundException("chess exercise with id " + exerciseId + " not found"));
        if (set.getExercises()
                .stream()
                .anyMatch(e -> e.getId().equals(chessExercise.getId()))
        ) {
            throw new ResourceAlreadyExistsException("chess exercise with id " + exerciseId + " already exists in set with id " + setId);
        }
        set.getExercises().add(chessExercise);
        save(set);
        return set;
    }

    public ChessExerciseSet update(Long id, ChessExerciseSet set) {
        if (id == null) {
            throw new InvalidResourceException("Id should not be null");
        }
        ChessExerciseSet updatedSet = chessExerciseSetRepository.findById(id)
                .map(setFromDatabase -> update(setFromDatabase, set))
                .orElseThrow(() -> new ResourceNotFoundException("Set with id " + id + " does not exist"));

        return chessExerciseSetRepository.save(updatedSet);
    }

    public ChessExerciseSet patch(Long id, ChessExerciseSet set) {
        if (id == null) {
            throw new InvalidResourceException("Id should not be null");
        }

        ChessExerciseSet updatedSet = chessExerciseSetRepository.findById(id)
                .map(setFromDatabase -> patch(setFromDatabase, set))
                .orElseThrow(() -> new ResourceNotFoundException("Set with id " + id + " does not exist"));

        return chessExerciseSetRepository.save(updatedSet);
    }

    private ChessExerciseSet update(ChessExerciseSet toUpdate, ChessExerciseSet from) {
        toUpdate.setName(from.getName());
        toUpdate.setDescription(from.getDescription());
        toUpdate.setExercises(from.getExercises());
        return toUpdate;
    }

    private ChessExerciseSet patch(ChessExerciseSet toUpdate, ChessExerciseSet from) {
        if (from.getName() != null) {
            toUpdate.setName(from.getName());
        }
        if (from.getDescription() != null) {
            toUpdate.setDescription(from.getDescription());
        }
        if (from.getExercises() != null && from.getExercises().size() != 0) {
            Set<Long> exercisesIdsFromSet = toUpdate.getExercises()
                    .stream()
                    .map(ChessExercise::getId)
                    .collect(Collectors.toSet());
            List<ChessExercise> exercisesToAdd = from.getExercises()
                    .stream()
                    .filter(exercise -> !exercisesIdsFromSet.contains(exercise.getId()))
                    .collect(Collectors.toList());

            toUpdate.getExercises().addAll(exercisesToAdd);
        }
        return toUpdate;
    }

}

package pl.edu.amu.chessproject.domain.port;

import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;

import java.util.List;
import java.util.Optional;

public interface ChessExerciseSetRepository {
    Optional<ChessExerciseSet> findById(Long id);

    List<ChessExerciseSet> findAll();

    ChessExerciseSet save(ChessExerciseSet chessExerciseSet);
}

package pl.edu.amu.chessproject.domain;

import org.springframework.stereotype.Service;
import pl.edu.amu.chessproject.domain.port.ChessExerciseRepository;
import pl.edu.amu.chessproject.domain.exceptions.InvalidResourceException;
import pl.edu.amu.chessproject.domain.exceptions.ResourceNotFoundException;
import pl.edu.amu.chessproject.domain.model.ChessExercise;

import java.util.List;

@Service
public class ChessExerciseService {

    private final ChessExerciseRepository chessExerciseRepository;

    public ChessExerciseService(ChessExerciseRepository chessExerciseRepository) {
        this.chessExerciseRepository = chessExerciseRepository;
    }

    public ChessExercise findById(Long id) {
        return chessExerciseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Chess exercise with id: " + id + " does not exist"));
    }

    public List<ChessExercise> findAll() {
        return chessExerciseRepository.findAll();
    }

    public ChessExercise update(Long id, ChessExercise chessExercise) {
        if (id == null) {
            throw new InvalidResourceException("Id should not be null");
        }
        ChessExercise updatedChessExercise = chessExerciseRepository.findById(id)
                .map(exerciseFromDatabase -> update(exerciseFromDatabase, chessExercise))
                .orElseThrow(() -> new ResourceNotFoundException("Chess exercise with id " + id + " does not exist"));
        return chessExerciseRepository.save(updatedChessExercise);
    }

    public ChessExercise save(ChessExercise chessExercise) {
        if (chessExercise.getId() != null) {
            throw new InvalidResourceException("Id should be null");
        }
        return chessExerciseRepository.save(chessExercise);
    }

    private ChessExercise update(ChessExercise toUpdate, ChessExercise from) {
        toUpdate.setName(from.getName());
        toUpdate.setDescription(from.getDescription());
        toUpdate.setSolution(from.getSolution());
        toUpdate.setFen(from.getFen());
        toUpdate.setTags(from.getTags());
        toUpdate.setDifficulty(from.getDifficulty());
        return toUpdate;
    }
}

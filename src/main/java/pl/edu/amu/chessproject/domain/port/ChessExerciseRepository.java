package pl.edu.amu.chessproject.domain.port;

import pl.edu.amu.chessproject.domain.model.ChessExercise;

import java.util.List;
import java.util.Optional;

public interface ChessExerciseRepository {
    Optional<ChessExercise> findById(Long id);

    List<ChessExercise> findAll();

    ChessExercise save(ChessExercise chessExercise);
}

package pl.edu.amu.chessproject.domain.port;

import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.model.Piece;

import java.awt.image.BufferedImage;

public interface ChessPiecesProvider {
    BufferedImage getPieceImage(Piece piece, ChessBoardSize size);
}

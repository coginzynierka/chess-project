package pl.edu.amu.chessproject.domain;

import org.springframework.stereotype.Service;
import pl.edu.amu.chessproject.domain.model.ChessBoard;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.model.Piece;
import pl.edu.amu.chessproject.domain.model.fen.Fen;
import pl.edu.amu.chessproject.domain.model.fen.ToMove;
import pl.edu.amu.chessproject.domain.port.ChessPiecesProvider;

import java.awt.*;
import java.awt.image.BufferedImage;

@Service
public class ChessBoardImageService {
    private final ChessPiecesProvider chessPiecesProvider;

    public ChessBoardImageService(ChessPiecesProvider chessPiecesProvider) {
        this.chessPiecesProvider = chessPiecesProvider;
    }

    public ChessBoardImageBuilder builder(ChessBoardSize chessBoardSize) {
        return new ChessBoardImageBuilder(chessBoardSize, chessPiecesProvider);
    }

    public static class ChessBoardImageBuilder {
        private final BufferedImage bufferedImage;
        private final Graphics2D board;
        private final ChessBoardSize size;
        private final ChessPiecesProvider chessPiecesProvider;
        private String id;

        ChessBoardImageBuilder(ChessBoardSize size, ChessPiecesProvider chessPiecesProvider) {
            this.size = size;
            this.chessPiecesProvider = chessPiecesProvider;
            this.bufferedImage = createBoardBufferedImage();
            this.board = bufferedImage.createGraphics();

            this.fillEmptyBoard();
        }

        public ChessBoardImageBuilder fromFen(Fen fen) {
            ToMove toMove = fen.getToMove();
            String fenBoardPosition = ToMove.WHITE.equals(toMove) ?
                    fen.getAbsoluteFenPosition() :
                    fen.getReversedFenPosition();
            String[] lines = fenBoardPosition.split("/");

            int x = size.getBoardLeftMargin();
            int y = size.getBoardTopMargin();

            for (int i = 0; i < lines.length; i++) {
                String line = lines[i];
                for (int j = 0; j < line.length(); j++) {
                    char c = line.charAt(j);
                    if (Character.isDigit(c)) {
                        int value = Integer.parseInt(String.valueOf(c));
                        x += value * size.getFieldWidth();
                    } else {
                        Piece piece = Piece.valueOf(String.valueOf(c));
                        addPiece(x, y, piece);
                        x += size.getFieldWidth();
                    }
                }
                x = size.getBoardLeftMargin();
                y += size.getFieldHeight();
            }
            addToMoveIcon(toMove);
            return this;
        }

        public ChessBoardImageBuilder id(String id) {
            this.id = id;
            return this;
        }

        public ChessBoard build() {
            return new ChessBoard(id, bufferedImage, size);
        }

        private void addPiece(int x, int y, Piece piece) {
            BufferedImage pieceImage = chessPiecesProvider.getPieceImage(piece, size);
            board.drawImage(pieceImage, x, y, null);
        }

        private BufferedImage createBoardBufferedImage() {
            return new BufferedImage(
                    size.getWholeBoardWidth(),
                    size.getWholeBoardHeight(),
                    BufferedImage.TYPE_INT_RGB);
        }

        private void fillEmptyBoard() {
            board.setColor(Color.GRAY);
            board.fillRect(
                    0,
                    0,
                    size.getWholeBoardWidth() + (size.getBoardLeftMargin() * 2),
                    size.getWholeBoardHeight() + (size.getBoardTopMargin() * 2)
            );

            board.setColor(new Color(181, 136, 99)); // black field color
            board.fillRect(
                    size.getBoardLeftMargin(),
                    size.getBoardTopMargin(),
                    size.getBoardWidth(),
                    size.getBoardHeight());

            board.setColor(new Color(240, 217, 181)); // white field color
            for (int i = size.getBoardTopMargin(); i < size.getBoardHeight(); i += size.getFieldHeight()) {
                for (int j = size.getBoardLeftMargin(); j < size.getBoardWidth(); j += size.getFieldWidth() * 2) {
                    board.fillRect(isEvenRow(i) ? j + size.getFieldWidth() : j, i, size.getFieldWidth(), size.getFieldHeight());
                }
            }
        }

        private void addToMoveIcon(ToMove toMove) {
            Color toMoveColor = ToMove.WHITE.equals(toMove) ?
                    new Color(255, 255, 255) :
                    new Color(0, 0, 0);
            board.setColor(toMoveColor);
            board.fillOval(
                    size.getBoardWidth() + size.getBoardLeftMargin() + 5,
                    size.getBoardHeight() + size.getBoardTopMargin() + 5,
                    size.getBoardLeftMargin() - 10,
                    size.getBoardTopMargin() - 10);
        }

        private boolean isEvenRow(int i) {
            return ((i - size.getBoardTopMargin()) / size.getFieldHeight()) % 2 == 1;
        }
    }
}

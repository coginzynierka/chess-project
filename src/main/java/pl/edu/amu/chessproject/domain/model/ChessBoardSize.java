package pl.edu.amu.chessproject.domain.model;

public enum ChessBoardSize {
    MEDIUM(42, 42, 68, 68); // 544 x 544, 628 x 628

    private int boardLeftMargin;
    private int boardTopMargin;
    private int fieldWidth;
    private int fieldHeight;
    private int boardWidth;
    private int boardHeight;
    private int wholeBoardWidth;
    private int wholeBoardHeight;


    ChessBoardSize(int boardLeftMargin, int boardTopMargin, int fieldWidth, int fieldHeight) {
        this.boardLeftMargin = boardLeftMargin;
        this.boardTopMargin = boardTopMargin;
        this.fieldWidth = fieldWidth;
        this.fieldHeight = fieldHeight;
        this.boardWidth = 8 * fieldWidth;
        this.boardHeight = 8 * fieldHeight;
        this.wholeBoardWidth = 2 * boardLeftMargin + boardWidth;
        this.wholeBoardHeight = 2 * boardTopMargin + boardHeight;
    }

    public int getBoardLeftMargin() {
        return boardLeftMargin;
    }

    public int getBoardTopMargin() {
        return boardTopMargin;
    }

    public int getFieldWidth() {
        return fieldWidth;
    }

    public int getFieldHeight() {
        return fieldHeight;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public int getWholeBoardWidth() {
        return wholeBoardWidth;
    }

    public int getWholeBoardHeight() {
        return wholeBoardHeight;
    }
}

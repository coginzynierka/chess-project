package pl.edu.amu.chessproject.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.image.BufferedImage;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChessBoard {
    private String id;
    private BufferedImage board;
    private ChessBoardSize size;
}

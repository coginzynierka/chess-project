package pl.edu.amu.chessproject.domain.model;

public enum ChessExerciseSetRepresentationSizes {
    THREE_COLUMNS(3);

    private int columns;

    ChessExerciseSetRepresentationSizes(int columns) {
        this.columns = columns;
    }

    public int getColumns() {
        return columns;
    }
}

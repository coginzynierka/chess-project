package pl.edu.amu.chessproject.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.edu.amu.chessproject.domain.model.fen.Fen;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChessExercise {
    private Long id;
    private String name;
    private String description;
    private String solution;
    private Fen fen;
    private Difficulty difficulty;
    private List<ChessExerciseTag> tags;

    public enum Difficulty {
        VERY_EASY,
        EASY,
        MEDIUM,
        INTERMEDIATE,
        HARD;

        @JsonValue
        public int toValue() {
            return ordinal();
        }
    }
}

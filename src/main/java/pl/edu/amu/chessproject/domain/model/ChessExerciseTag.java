package pl.edu.amu.chessproject.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChessExerciseTag {
    private Long id;
    private String name;
    private String description;

    public ChessExerciseTag(String name, String description) {
        this.name = name;
        this.description = description;
    }
}

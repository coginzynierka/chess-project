package pl.edu.amu.chessproject.domain;

import org.springframework.stereotype.Service;
import pl.edu.amu.chessproject.domain.model.ChessBoard;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.model.ChessExercise;
import pl.edu.amu.chessproject.domain.model.fen.Fen;
import pl.edu.amu.chessproject.domain.port.ChessBoardRepository;

@Service
public class ChessBoardService {
    private final ChessBoardRepository chessBoardRepository;
    private final ChessBoardImageService chessBoardImageService;

    public ChessBoardService(ChessBoardRepository chessBoardRepository, ChessBoardImageService chessBoardImageService) {
        this.chessBoardRepository = chessBoardRepository;
        this.chessBoardImageService = chessBoardImageService;
    }

    public ChessBoard saveChessBoard(ChessBoard chessBoard) {
        return chessBoardRepository.save(chessBoard);
    }

    public ChessBoard getOrCreateFromChessExercise(ChessExercise chessExercise, ChessBoardSize size) {
        return chessBoardRepository.findByIdAndSize(chessExercise.getId().toString(), size)
                .orElseGet(() -> {
                    Fen fen = chessExercise.getFen();
                    String id = chessExercise.getId().toString();

                    ChessBoard chessBoard = chessBoardImageService.builder(size)
                            .id(id)
                            .fromFen(fen)
                            .build();
                    return saveChessBoard(chessBoard);
                });
    }
}

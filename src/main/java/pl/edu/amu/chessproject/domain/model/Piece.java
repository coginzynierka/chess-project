package pl.edu.amu.chessproject.domain.model;

public enum Piece {
    K("white-king"),
    k("black-king"),
    Q("white-queen"),
    q("black-queen"),
    R("white-rook"),
    r("black-rook"),
    B("white-bishop"),
    b("black-bishop"),
    N("white-knight"),
    n("black-knight"),
    P("white-pawn"),
    p("black-pawn");

    private String fullName;

    Piece(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getFenName() {
        return toString();
    }
}

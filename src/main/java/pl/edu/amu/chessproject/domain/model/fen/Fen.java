package pl.edu.amu.chessproject.domain.model.fen;

import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public class Fen {
    private final String fen;
    private final ToMove toMove;
    @Getter(value = AccessLevel.NONE)
    private final String[] fenSections;

    public Fen(String fen) {
        this.fen = fen;
        this.fenSections = fen.split(" ");
        this.toMove = calculateToMove();
    }

    public String getAbsoluteFenPosition() {
        return fenSections[0];
    }

    /*
     * blacks perspective
     */
    public String getReversedFenPosition() {
        return StringUtils.reverse(getAbsoluteFenPosition());
    }

    private ToMove calculateToMove() {
        return "w".equals(fenSections[1]) ? ToMove.WHITE : ToMove.BLACK;
    }
}

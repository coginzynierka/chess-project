package pl.edu.amu.chessproject.domain.model;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;

public class DocumentWrapper {
    private Document document;
    private ByteArrayOutputStream byteArrayOutputStream;

    public DocumentWrapper(Document document) {
        try {
            this.document = document;
            this.byteArrayOutputStream = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, byteArrayOutputStream);
        } catch (DocumentException e) {
            System.out.println("zjebalo sie");
            e.printStackTrace();
        }
    }

    public Document getDocument() {
        return document;
    }

    public byte[] toByteArray() {
        return byteArrayOutputStream.toByteArray();
    }
}

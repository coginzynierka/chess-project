package pl.edu.amu.chessproject.domain.port;

import pl.edu.amu.chessproject.domain.model.ChessBoard;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;

import java.util.Optional;

public interface ChessBoardRepository {
    ChessBoard save(ChessBoard chessBoard);

    Optional<ChessBoard> findByIdAndSize(String id, ChessBoardSize size);
}

package pl.edu.amu.chessproject.domain;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import pl.edu.amu.chessproject.domain.model.ChessBoard;
import pl.edu.amu.chessproject.domain.model.ChessBoardSize;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSet;
import pl.edu.amu.chessproject.domain.model.ChessExerciseSetRepresentationSizes;
import pl.edu.amu.chessproject.domain.model.DocumentWrapper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class ChessExerciseSetPdfRepresentationService {

    private ChessBoardService chessBoardService;

    public ChessExerciseSetPdfRepresentationService(ChessBoardService chessBoardService) {
        this.chessBoardService = chessBoardService;
    }

    public DocumentWrapper createPdfDocumentFromSet(ChessExerciseSet set, ChessExerciseSetRepresentationSizes size) {
        Document document = new Document(PageSize.A4, 0, 0, 20, 0);
        DocumentWrapper documentWrapper = new DocumentWrapper(document);
        PdfPTable table = createCustomizedPdfPTable(size);

        fillTableBasedOnSet(set, table);

        try {
            document.open();

            addTitle(set, document);
            addDescription(set, document);
            document.add(table);

            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return documentWrapper;
    }

    private void addTitle(ChessExerciseSet set, Document document) throws DocumentException {
        Paragraph documentTitle = new Paragraph(set.getName());
        documentTitle.setAlignment(Element.ALIGN_CENTER);
        document.add(documentTitle);
    }

    private void addDescription(ChessExerciseSet set, Document document) throws DocumentException {
        if (StringUtils.isNotEmpty(set.getDescription())) {
            Paragraph documentDescription = new Paragraph(set.getDescription(), new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.ITALIC));
            documentDescription.setAlignment(Element.ALIGN_CENTER);
            document.add(documentDescription);
        }
    }

    private void fillTableBasedOnSet(ChessExerciseSet set, PdfPTable table) {
        set.getExercises().forEach(exercise -> {
            ChessBoard board = chessBoardService.getOrCreateFromChessExercise(exercise, ChessBoardSize.MEDIUM);
            Image boardImage = mapToImage(board.getBoard());
            boardImage.setAlignment(Element.ALIGN_CENTER);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.setFixedHeight(260);
            pdfPCell.setPadding(20f);
            pdfPCell.addElement(boardImage);

            Paragraph name = new Paragraph(exercise.getName());
            Paragraph description = new Paragraph(exercise.getDescription());
            name.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 10));
            description.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 10));
            pdfPCell.addElement(name);
            pdfPCell.addElement(description);
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            table.addCell(pdfPCell);
        });

        table.completeRow();
    }

    private PdfPTable createCustomizedPdfPTable(ChessExerciseSetRepresentationSizes size) {
        PdfPTable table = new PdfPTable(size.getColumns()) {
            @Override
            public void completeRow() {
                while (!rowCompleted) {
                    addCell(getDefaultCell());
                }
            }

            @Override
            public PdfPCell getDefaultCell() {
                PdfPCell defaultCell = super.getDefaultCell();
                defaultCell.setBorder(Rectangle.NO_BORDER);
                return defaultCell;
            }
        };
        table.setWidthPercentage(100f);
        return table;
    }

    private Image mapToImage(BufferedImage board) {
        try {
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            ImageIO.write(board, "png", byteArray);
            return Image.getInstance(byteArray.toByteArray());
        } catch (IOException | BadElementException e) {
            System.out.println("zjebalo sie");
            e.printStackTrace();
        }
        return null;
    }

}

INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Mat w 4', '',	'1r4k1/4q1p1/6Qp/1r1p4/p1nP2P1/P1P1pR1P/KP2R3/2B5 b - - 0 33', 0, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Odciągnięcie', '',	'8/8/3k4/R4p1P/2pB2r1/3bK3/P7/8 b - - 2 49', 1, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Odciagnięcie', 'Czy po .. Bf2 + dobre jest Ke2?', 'r4rk1/pp1b1ppp/1qn2n2/2b1p3/4P3/P1NB1N1P/1PPB2P1/R2QK2R w KQ - 1 13', 2, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Widełki', '',	'6rr/2pk1p2/2p5/4p2p/3nP3/2QP1RP1/P4P2/R5K1 b - - 0 27', 3, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Mat w 2', '',	'8/6kp/1r2rR2/4P1B1/p1p5/1bN2P2/1Pn2K2/8 w - - 0 39', 4, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Mat w 2', '',	'5rk1/1p1b1pp1/1Q2p2p/p2pP3/P1rn3P/1P1N4/5PP1/R4RK1 b - - 0 25', 4, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Mat w 4', '',	'1q3r1k/3R2p1/4Q2p/2p1N3/8/8/r4nPP/5RK1 b - - 0 29', 2, '');
INSERT INTO chess_exercise (name, description, fen, difficulty, solution) VALUES ('Przynęta & Widełki', 'Chess Olympiad 1992, Gulsevil (2030) vs Bednarska (2150)', '8/3rkbpp/1b3p2/np2pP2/2p3P1/2P1P2P/3NB3/R2NK3 b -KQkq 0 1', 3, '');

INSERT INTO chess_exercise_tag (name) VALUES ('Odciagniecie'), ('Widelki'), ('Mat w 4'), ('Mat w 2');

INSERT INTO chess_exercise_tags (chess_exercise_dto_id, tags_id) VALUES (1, 3), (2, 2), (3, 2), (4, 3), (5, 4), (6, 4);

INSERT INTO chess_exercise_set (name, description) VALUES ('pierwszy zestaw', 'tutaj jest opis');

INSERT INTO chess_exercise_set_exercises (chess_exercise_set_dto_id, exercises_id) VALUES (1, 1), (1, 2), (1, 3), (1, 4);

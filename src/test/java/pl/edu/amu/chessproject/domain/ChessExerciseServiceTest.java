package pl.edu.amu.chessproject.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import pl.edu.amu.chessproject.domain.port.ChessExerciseRepository;
import pl.edu.amu.chessproject.domain.exceptions.InvalidResourceException;
import pl.edu.amu.chessproject.domain.exceptions.ResourceNotFoundException;
import pl.edu.amu.chessproject.domain.model.ChessExercise;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class ChessExerciseServiceTest {

    private ChessExerciseService chessExerciseService;
    private ChessExerciseRepository chessExerciseRepository;

    @Before
    public void init() {
        this.chessExerciseRepository = Mockito.mock(ChessExerciseRepository.class);
        this.chessExerciseService = new ChessExerciseService(chessExerciseRepository);
    }

    @Test
    public void shouldReturnChessExerciseWhenRepositoryReturnsChessExercise() {
        //given
        long id = 1;
        ChessExercise expected = ChessExercise.builder()
                .id(id)
                .name("Test exercise")
                .build();

        stubRepositoryFindById(expected);

        //when
        ChessExercise actualExercise = chessExerciseService.findById(id);

        //then
        assertEquals("Service return invalid exercise", expected, actualExercise);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void shouldThrowResourceNotFoundExceptionWhenRepositoryReturnsNull() {
        //given
        long id = 1;
        stubRepositoryFindById(null);

        //when
        chessExerciseService.findById(id);
    }

    @Test
    public void shouldReturnListOfChessExercisesWhenRepositoryReturnList() {
        //given
        List<ChessExercise> expectedList = Arrays.asList(
                ChessExercise.builder().id(1L).build(),
                ChessExercise.builder().id(2L).build(),
                ChessExercise.builder().id(3L).build()
        );
        stubRepositoryFindAll(expectedList);

        //when
        List<ChessExercise> actual = chessExerciseService.findAll();

        //then
        assertEquals("Return list is different than expected", expectedList, actual);

    }

    @Test
    public void saveChessExerciseShouldCallAdapterSaveMethod() {
        //given
        ChessExercise inputExercise =
                ChessExercise.builder()
                        .name("First chess exercise")
                        .description("Lorem ipsum")
                        .build();
        ChessExercise expected =
                ChessExercise.builder()
                        .id(1L)
                        .name("First chess exercise")
                        .description("Lorem ipsum")
                        .build();

        stubRepositorySave(expected);

        //when
        ChessExercise actualExercise = chessExerciseService.save(inputExercise);

        //then
        assertEquals(expected, actualExercise);
        Mockito.verify(chessExerciseRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test(expected = InvalidResourceException.class)
    public void saveChessExerciseShouldThrowAnException() {
        //given
        ChessExercise exercise = ChessExercise.builder()
                .id(1L)
                .name("First chess exercise")
                .build();

        //when
        chessExerciseService.save(exercise);
    }

    private void stubRepositoryFindById(ChessExercise chessExercise) {
        Mockito.when(chessExerciseRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(chessExercise));
    }

    private void stubRepositoryFindAll(List<ChessExercise> chessExercises) {
        Mockito.when(chessExerciseRepository.findAll()).thenReturn(chessExercises);
    }

    private void stubRepositorySave(ChessExercise chessExercise) {
        Mockito.when(chessExerciseRepository.save(Mockito.any())).thenReturn(chessExercise);
    }
}
